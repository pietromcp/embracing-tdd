namespace EmbracingTdd {
    public class Greeter {
        private readonly string message;

        public Greeter(string message) {
            this.message = message;
        }
        
        public Greeter(): this("Hello") {
        }

        public string SayHello() {
            return SayHello("World");
        }

        public string SayHello(string to) {
            return $"{message}, {to}!";
        }
    }
}