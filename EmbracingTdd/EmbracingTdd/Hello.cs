﻿using System;

namespace EmbracingTdd {
    public class Hello {
        public string SayHello() => "Hello, World!";
    }
}