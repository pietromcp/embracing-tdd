using EmbracingTdd;
using NHamcrest;
using Xunit;
using Assert = NHamcrest.XUnit.Assert;

namespace EmbracingTddTests {
    public class GreeterTest {
        [Fact]
        public void SayHelloWorld() {
            var message = new Greeter().SayHello();
            Assert.That(message, Is.EqualTo("Hello, World!"));
        }

        [Fact]
        public void CanSpecifyTarget() {
            var message = new Greeter().SayHello("Pietro");
            Assert.That(message, Is.EqualTo("Hello, Pietro!"));
        }

        [Fact]
        public void CanSpecifyBaseMessage() {
            var message = new Greeter("¡Hola").SayHello("Pedro");
            Assert.That(message, Is.EqualTo("¡Hola, Pedro!"));
        }
    }
}