using System;
using EmbracingTdd;
using NHamcrest;
using Xunit;
using Assert = NHamcrest.XUnit.Assert;

namespace EmbracingTddTests {
    public class HelloTest {
        [Fact]
        public void ShouldSayHelloWorld() {
            Assert.That(new Hello().SayHello(), Is.EqualTo("Hello, World!"));;
        }
    }
}